<?php
// HTTP
define('HTTP_SERVER', 'http://orzon.loc:8080/');

// HTTPS
define('HTTPS_SERVER', 'http://orzon.loc:8080/');

// DIR
define('DIR_APPLICATION', 'E:/OpenServer/domains/samarqand/orzon/catalog/');
define('DIR_SYSTEM', 'E:/OpenServer/domains/samarqand/orzon/system/');
define('DIR_IMAGE', 'E:/OpenServer/domains/samarqand/orzon/image/');
define('DIR_LANGUAGE', 'E:/OpenServer/domains/samarqand/orzon/catalog/language/');
define('DIR_TEMPLATE', 'E:/OpenServer/domains/samarqand/orzon/catalog/view/theme/');
define('DIR_CONFIG', 'E:/OpenServer/domains/samarqand/orzon/system/config/');
define('DIR_CACHE', 'E:/OpenServer/domains/samarqand/orzon/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:/OpenServer/domains/samarqand/orzon/system/storage/download/');
define('DIR_LOGS', 'E:/OpenServer/domains/samarqand/orzon/system/storage/logs/');
define('DIR_MODIFICATION', 'E:/OpenServer/domains/samarqand/orzon/system/storage/modification/');
define('DIR_UPLOAD', 'E:/OpenServer/domains/samarqand/orzon/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
