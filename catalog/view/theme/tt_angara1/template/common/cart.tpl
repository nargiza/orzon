<div id="cart" class="cart">
    <a href="<?php echo $cart; ?>">
        <div class="cart-button">
            <img src="catalog/view/orzon/images/shopping-basket.png">
            <div id="cart-total" class="number-cart"><span><?php echo $text_items; ?></span></div>
        </div>
    </a>
</div>