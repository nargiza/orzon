<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>


	<link rel="stylesheet" href="catalog/view/orzon/owlcarousel/owl.carousel.min.css" />
	<link rel="stylesheet" href="catalog/view/orzon/owlcarousel/owl.theme.default.css" />
	<link rel="stylesheet" href="catalog/view/orzon/style/bootstrap.min.css">
	<link rel="stylesheet" href="catalog/view/orzon/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="catalog/view/orzon/style/style.css">
	<script src="catalog/view/orzon/js/jquery-3.1.1.min.js"></script>
	<script src="catalog/view/orzon/js/main.js"></script>
	<script src="catalog/view/orzon/owlcarousel/jquery.min.js"></script>
	<script src="catalog/view/orzon/owlcarousel/owl.carousel.min.js"></script>
	<script src='catalog/view/orzon/js/jquery.zoom.js'></script>




	<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body>
<header>
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 left-block text-left">
					<a href="#">Условия доставка</a>
					<a href="#">Условия оплаты</a>
					<a href="#">Помощь</a>
				</div>
				<div class="col-lg-6 right-block text-right">
					<div class="account">
						<a href="#" class="enter">Войти</a>
						<span>/</span>
						<a href="#">Регистрация</a>
					</div>

					<?php echo $language; ?>

					<div class="authorization">
						<h1>Авторизация<div class="close-popup"></div></h1>
						<input type="tel" name="phone" placeholder="+998 _________" required>
						<input type="text" name="text" placeholder="Ваш пароль"><div class="lock"></div>
						<div class="button-go">
							<input class="red-btn " type="submit" name="Войти" value="Войти">
							<a href="#">Забыли пароль?</a>
						</div>
						<div class="rigistration">
							<span>если у вас еще нет учетной записи, <a href="#">зарегистрируйтесь</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="header-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<div class="logo">
						<?php if ($logo) { ?>
						<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
						<?php } else { ?>
						<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
						<?php } ?>
					</div>

				</div>
				<div class="col-lg-5">
					<?php echo $search; ?>

				</div>
				<div class="col-lg-5 right-block">
					<div class="time-work">
						<span><i class="fa fa-clock-o" aria-hidden="true"></i>Без выходных</span>
						<span>9.00 - 21.00</span>
					</div>
					<div class="call-number">
						<span><i class="fa fa-phone" aria-hidden="true"></i>+ 998 90 200 00 00</span>
						<a class="open-popup-call" href="#">Заказать звонок</a>
					</div>
					<div class="feedback">
						<div class="popup-call">
							<div class="close-popup"></div>
							<h1>Заказать звонок</h1>
							<input type="text" name="your-name" placeholder="Ваше имя">
							<input type="tel" name="your-tel" placeholder="Ваш телефон">
							<input class="message" type="text" name="" placeholder="Ваш вопрос">
							<input class="red-btn order-button" type="submit" name="заказать" value="Заказать">
						</div>
						<div class="return-message">
							<div class="close-popup-message"></div>
							<h2>Спасибо</h2>
							<span>Ваша заявка принята, в течения 30 минут с вами свяжется наш менеджер</span>
						</div>
					</div>

					<?php echo $cart; ?>

				</div>
			</div>
		</div>
	</div>
	<div class="nav-menu">
		<div class="container">
			<div class="row">
			<?php if(isset($block1)){ echo $block1; }?>
			</div>
		</div>
	</div>

	<div class="header-fixed">
		<div class="container">
			<div class="row">
				<div class="col-lg-1 logo-fix">
					<a href="index.html"><img src="catalog/view/orzon/images/logo-fix.png"></a>
				</div>
				<div class="col-lg-2" style="width: 22%;">
					<div class="nav-menu">
						<div class="menu">
								<span href="#" class="product-catalog prod-catal-fixed">
									<img src="catalog/view/orzon/images/gamburger.png">
									<span>Каталог товаров</span>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</span>
						</div>
						<div class="product-catalog-menu prod-catal-menu-fixed">
							<ul class="main-menu">
								<li><a href="#" class="open-menu-item">Продукты питания<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Ресторан на дому<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Продукты питания<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Бытовая техника<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Товары для дома<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Все для ремонта<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Красота и здаровья<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Зоотовары<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Детские товары<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="open-menu-item">Концелярские товары<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="color-stock">Акции<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="scolor-hit">Хит продаж<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
								<li><a href="#" class="color-new">Новинки<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
							</ul>
							<ul class="item-menu">
								<ul>
									<li class="column">
										<ul>
											<li><a href="#" class="title">Телефоны</a></li>
											<li><a href="#">Смартфоны</a></li>
											<li><a href="#">Мобильные телефоны</a></li>
											<li><a href="#">Смарт-часы</a></li>
											<li><a href="#">Чехлы и бамперы</a></li>
											<li><a href="#">Аксессуары для смартфонов</a></li>
											<li><a href="#">Все телефоны<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
										</ul>
									</li>
									<li class="column">
										<ul>
											<li><a href="#" class="title">Аксессуары</a></li>
											<li><a href="#">Чехлы и сумки</a></li>
											<li><a href="#">Картриджи и тонеры</a></li>
											<li><a href="#">Внешние аккумуляторы</a></li>
											<li><a href="#">Карты памяти</a></li>
											<li><a href="#">Внешние жесткие диски</a></li>
											<li><a href="#">Кабели и переходники</a></li>
											<li><a href="#">Все телефоны<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
										</ul>
									</li>
									<li class="column">
										<ul>
											<li><a href="#" class="title">Фото и видео</a></li>
											<li><a href="#">Зеркальные фотоаппараты</a></li>
											<li><a href="#">Цифровые фотоаппараты</a></li>
											<li><a href="#">Экшн-камеры</a></li>
											<li><a href="#">Видеокамеры</a></li>
											<li><a href="#">Для фотостудий</a></li>
											<li><a href="#">Аксессуары для фото</a></li>
											<li><a href="#">Все телефоны<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
										</ul>
									</li>
									<br>
									<li class="column">
										<ul>
											<li><a href="#" class="title">Телефоны</a></li>
											<li><a href="#">Смартфоны</a></li>
											<li><a href="#">Мобильные телефоны</a></li>
											<li><a href="#">Смарт-часы</a></li>
											<li><a href="#">Чехлы и бамперы</a></li>
											<li><a href="#">Аксессуары для смартфонов</a></li>
											<li><a href="#">Все телефоны<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
										</ul>
									</li>
									<li class="column">
										<ul>
											<li><a href="#" class="title">Аксессуары</a></li>
											<li><a href="#">Чехлы и сумки</a></li>
											<li><a href="#">Картриджи и тонеры</a></li>
											<li><a href="#">Внешние аккумуляторы</a></li>
											<li><a href="#">Карты памяти</a></li>
											<li><a href="#">Внешние жесткие диски</a></li>
											<li><a href="#">Кабели и переходники</a></li>
											<li><a href="#">Все телефоны<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
										</ul>
									</li>
									<li class="column column-banner">
										<ul>
											<li>
												<a href="#">
													<img src="catalog/view/orzon/images/menu-ban2.png">
												</a>
											</li>
											<li>
												<a href="#">
													<img src="catalog/view/orzon/images/menu-ban.png">
												</a>
											</li>
										</ul>
									</li>
									<br>
									<li class="column">
										<div class="link">
											<a href="#" class="link-menu">Сантехника и оснащение</a>
											<a href="#" class="link-menu">Товары для новорожденных</a>
											<a href="#" class="link-menu">Уход и гигиена</a>
										</div>
									</li>
									<li class="column">
										<div class="link">
											<a href="#" class="link-menu">Сантехника и оснащение</a>
											<a href="#" class="link-menu">Товары для новорожденных</a>
										</div>
									</li>
								</ul>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-5 search-fix">
					<div class="search-form">
						<input class="form-control"placeholder="Поиск по каталогу">
						<a class="all-categories">
							Все разделы
							<i class="fa fa-angle-down" aria-hidden="true"></i>
							<div class="menu-all-categor">
								<ul>
									<li><a href="#" class="all">Все разделы</a></li>
									<li><a href="#">Продукты питания</a></li>
									<li><a href="#">Ресторан на дому</a></li>
									<li><a href="#">Продукты питания</a></li>
									<li><a href="#">Бытовая техника</a></li>
									<li><a href="#">Товары для дома</a></li>
								</ul>
								<ul>
									<li><a href="#">Все для ремонта</a></li>
									<li><a href="#">Красота и здоровье</a></li>
									<li><a href="#">Зоотовары</a></li>
									<li><a href="#">Детские товары</a></li>
									<li><a href="#">Концелярские товары</a></li>
								</ul>
							</div>
						</a>
						<button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</div>
				</div>
				<div class="col-lg-4 right-block-fix">
					<div class="account">
						<a href="#" class="enter">Войти</a>
						<span>/</span>
						<a href="#">Регистрация</a>
					</div>
					<a href="#" class="shares">
						<img src="catalog/view/orzon/images/Path.png">
						<i class="fa fa-percent" aria-hidden="true"></i>Акции
					</a>
					{*<?php echo $cart; ?>*}

				</div>
			</div>
		</div>
	</div>
</header>


<script type="text/javascript">
$(document).ready(function(){
	$(function dropDown()
	{
		elementClick = '#form-language .btn-language,#form-currency .btn-currency,#top-links .link-account,#cart .btn-cart';
		elementSlide =  '.dropdown-menu';
		activeClass = 'active';

		$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
		subUl.slideDown();
		$(this).addClass(activeClass);
		}
		else
		{
		subUl.slideUp();
		$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
		});

		$(elementSlide).on('click', function(e){
		e.stopPropagation();
		});

		$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
		});
	});
});
</script>